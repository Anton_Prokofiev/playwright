const { MainPage } = require('../framework')
const { chromium } = require('playwright');

describe("Noop spec", async function () {
    let browser = null;
    let page = null;
    beforeEach(async () => {
        browser = await chromium.launch({ headless: false });
        const context = await browser.newContext();
        page = await context.newPage();
        await page.goto('http://whatsmyuseragent.org/');

    })

    it("noop it", async function () {

        const mainPage = new MainPage(page)
        mainPage.login('test@gmail.com', 'pwd')
    })

    afterEach(async () => {
        await page.screenshot({ path: `example.png` });
        await browser.close();
    })
})